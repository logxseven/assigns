import nltk
from nltk.corpus import wordnet
from nltk.corpus import wordnet as wn

max = -1
bestSol = [] #contains best solution
p = 1 #similarity proba
phrase = ['rain','will','fall']

syns = [] # matrix .. lignes : nbWords-1 ... columns : nbSynonyms-1

# getting synsets of each word 
for i in range(0,len(phrase)): 
    syns.append(wn.synsets(phrase[i])) 

print("analyzing please wait")

#running trought the matrix and calculating proba of each 3 elements

for i in range(0,len(syns[0])):
    for j in range(0,len(syns[1])):
        if syns[0][i].path_similarity(syns[1][j]) is None:
            p*=0.0000001
        else:
            p *= syns[0][i].path_similarity(syns[1][j])
        for k in range(0,len(syns[2])):
            if syns[0][i].path_similarity(syns[2][k]) is None:
                p*=0.0000001
            else:
                p *= syns[0][i].path_similarity(syns[2][k])
            if syns[1][j].path_similarity(syns[2][k]) is None:
                p*=0.0000001
            else:
                p *= syns[1][j].path_similarity(syns[2][k])
            if p > max:
                max = p
                bestSol = [syns[0][i],syns[1][j],syns[2][k]]
            p = 1

#calculating similarity between the best Cases
proba = 1
proba *= bestSol[0].path_similarity(bestSol[1])
proba *= bestSol[0].path_similarity(bestSol[2])
proba *= bestSol[1].path_similarity(bestSol[2])

print("Best Solution is : " , bestSol)
print("With a propability of " , proba)
