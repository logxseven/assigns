/**
 *
 * @author hammi
 */

package javaapplication2;
import java.util.*;


public class car {
    public int numImmatriculation;
    public String Marque;
    public String Modele;
    public float Puissance;
    public float Prix;
    public float numSerie;
    
    
    public car(){}  
    public car(int numImmatriculation,String Marque,String Modele,float Puissance,float Prix,float numSerie) {
        this.numImmatriculation = numImmatriculation;
        this.Marque = Marque ;
        this.Modele = Modele;
        this.Puissance = Puissance;
        this.Prix = Prix;
        this.numSerie = numSerie;
    } 

    
    public int compteurMARQ (ArrayList<car> Liste){ 
        int Somme=0;
        for (int i=0;i<Liste.size();i++)
            if (Liste.get(i).Marque.equals("MARQ"))
                    Somme++;
        return Somme; 
    }
    
    public void ajouterVoiture (){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Donner numero d'immatriculation");
        this.numImmatriculation = sc.nextInt();
        System.out.println("Donner Marque");
        this.Marque = sc.next();
        System.out.println("Donner Modele");
        this.Modele = sc.next();
        System.out.println("Donner Puissance");
        this.Puissance = sc.nextFloat();
        System.out.println("Donner Prix");
        this.Prix = sc.nextFloat();
        System.out.println("Donner Numero de serie");
        this.numSerie = sc.nextFloat();
        
        //return new car(numImmatriculationScanner,MarqueScanner,ModeleScanner,PuissanceScanner,PrixScanner,numSerieScanner);
    }
    
    public static int chercherVoiture (ArrayList<car> Liste,int numImmatriculation){
        for (int i=0;i<Liste.size();i++)
            if (Liste.get(i).numImmatriculation == numImmatriculation) return i;
        System.out.println("Voiture Inexistante");
        return Liste.size()+1;
    }
    
    public car chercherVoiture (ArrayList<car> Liste,float Prix){
        for (int i=0;i<Liste.size();i++)
            if (Liste.get(i).Prix == Prix) return Liste.get(i);
        return null;
    }
    public car chercherVoiture (ArrayList<car> Liste,String Marque){
        for (int i=0;i<Liste.size();i++)
            if (Liste.get(i).Marque.equals(Marque)) return Liste.get(i);
        return null;
    }
  
    public String afficherVoiture (){
        return  "Numero d'Immatriculation : "+this.numImmatriculation
                +"\nMarque : "+this.Marque
                +"\nModele : "+this.Modele
                +"\nPuissance : "+this.Puissance
                +"\nPrix : "+this.Prix
                +"\nNumero de Serie : "+this.numSerie
                +"\n";
        
    }
    
    public static boolean supprimerVoiture(ArrayList <car> Liste,car VoitureVendu){
        for (int i=0;i<Liste.size();i++)
            if (Liste.get(i).numImmatriculation == VoitureVendu.numImmatriculation &&
                Liste.get(i).Modele.equals(VoitureVendu.Modele) &&
                Liste.get(i).Marque.equals(VoitureVendu.Marque) &&
                Liste.get(i).Prix == VoitureVendu.Prix &&
                Liste.get(i).Puissance == VoitureVendu.Puissance &&
                Liste.get(i).numSerie == VoitureVendu.numSerie){Liste.remove(i);return true;}
        
        return false;
    }
  
}

