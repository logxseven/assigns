package printersim;
/**
 *
 * @author mohamed
 */
public class Printer {
    public int Limite;
    public int nbPagePrinted; 
    
    public Printer (int Limite){
        this.Limite = Limite;
        this.nbPagePrinted = 0;
    }
        // en appelle la fonction pour chaque page .. 
    public void print (Utilisateur user){
        if (this.nbPagePrinted < this.Limite)  
            if (user.nbPage > user.nbPagePrinter){
                user.nbPagePrinter++;
                System.out.println("Impression avec succès");
                this.nbPagePrinted++;
            }
            else 
                System.out.println("Impression echoué, Limite de num d'impression pour Utilisateur" + user.TYPE);
        else 
            System.out.println("Impression echoué, Limite de num d'impression sur imprimante pour le mois");   
    }
}
