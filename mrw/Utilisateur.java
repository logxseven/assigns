
package printersim;

/**
 *
 * @author mohamed
 */
public class Utilisateur {
    public String TYPE;
    public int nbPage;
    public int nbPagePrinter;
        
    public Utilisateur (int TYPE){
        // choix type
        if (TYPE == 1)
            this.TYPE = "Chercheur";
        else if (TYPE == 2)
            this.TYPE = "Etudiant";
        else if (TYPE == 3)
            this.TYPE = "Directeur";
        
        // attribution limite
        if (TYPE == 1)
            this.nbPage = 200;
        else if (TYPE == 2)
            this.nbPage = 100;
        else 
            this.nbPage = 1000;
        
        // init page printé
        this.nbPagePrinter = 0;
    }

}
